# Kanboard container

Debian (Buster) based container for the [Kanboard](https://github.com/kanboard/kanboard) project.

## Build

The default Kanboard version installed in the container is the release *v1.2.10*.
The Kanboard container is built using the below command:

```
$ docker build -t kanboard .
```

You can change the Kanboard version at build time:
```
$ docker build --build-arg KANBOARD_VERSION=1.2.9 -t kanboard:1.2.9 .
```

The kanboard image is continuously built and pushed on the Docker Hub: `aallrd/kanboard`

## Usage

In order to start the Kanboard container, run the below command:

```
$ docker run -d --name kanboard -P aallrd/kanboard
```

The ports *80* and *443* are exposed by default by the container on the host.
In order to get the corresponding random ports assigned on the host, you can run the below command:

```
$ docker port $(docker ps | grep kanboard | awk '{print $1}')
443/tcp -> 0.0.0.0:32826
80/tcp -> 0.0.0.0:32827
```

You can also chose to start the Kanboard container by binding to specific ports:
```
$ docker run -d --name kanboard -p 80:80 -p 443:443 aallrd/kanboard
```

In order to test that the application is running successfully:
* http: `curl -L http://0.0.0.0:32826`
* https: `curl -kL https://0.0.0.0:32826`

## Configuration

The Kanboard container configuration can be changed at runtime using:
* [environment variables](https://docs.kanboard.org/en/latest/admin_guide/docker.html#environment-variables)
* [volumes](https://docs.kanboard.org/en/latest/admin_guide/docker.html#volumes)

## Cluster integration

These exemples are taken from the [Kanboard Docker Admin Guide](https://docs.kanboard.org/en/latest/admin_guide/docker.html).

### Example with Sqlite

```
version: '2'
services:
  kanboard:
    image: aallrd/kanboard:latest
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - kanboard_data:/var/www/app/data
      - kanboard_plugins:/var/www/app/plugins
      - kanboard_ssl:/etc/nginx/ssl
volumes:
  kanboard_data:
    driver: local
  kanboard_plugins:
    driver: local
  kanboard_ssl:
    driver: local
```

### Example with MariaDB:

```
version: '2'
services:
  kanboard:
    image: aallrd/kanboard:latest
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - kanboard_data:/var/www/app/data
      - kanboard_plugins:/var/www/app/plugins
      - kanboard_ssl:/etc/nginx/ssl
    environment:
      DATABASE_URL: mysql://kb:kb-secret@db/kanboard
  db:
    image: mariadb:latest
    command: --default-authentication-plugin=mysql_native_password
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: kanboard
      MYSQL_USER: kb
      MYSQL_PASSWORD: kb-secret
volumes:
  kanboard_data:
    driver: local
  kanboard_plugins:
    driver: local
  kanboard_ssl:
    driver: local
```

### Starting the container with Docker Compose:

```
$ docker-compose up
```
