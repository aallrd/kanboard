FROM debian:stretch
MAINTAINER mail@aallrd.com

# The kanboard project is providing a configuration based on the S6 supervision suite.
# The S6 binaries are not available directly on Debian Buster, so in order to stick
# as much as possible to the original project we are going to build them.
# This should be properly packaged at some point instead of using a multi-stage build.
# The build instructions are here:
# - https://skarnet.org/software/skalibs
# - https://skarnet.org/software/execline
# - https://skarnet.org/software/s6

ARG SKALIBS_VERSION=2.8.1.0
ARG EXECLINE_VERSION=2.5.1.0
ARG S6_VERSION=2.8.0.1

RUN apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests \
      build-essential \
      wget \
      ca-certificates \
    && wget -q https://skarnet.org/software/skalibs/skalibs-${SKALIBS_VERSION}.tar.gz \
    && tar -xzf skalibs-${SKALIBS_VERSION}.tar.gz \
    && cd skalibs-${SKALIBS_VERSION} \
    && ./configure --disable-shared \
    && make \
    && make install \
    && make install DESTDIR=/root/s6 \
    && cd .. \
    && wget -q https://skarnet.org/software/execline/execline-${EXECLINE_VERSION}.tar.gz \
    && tar -xzf execline-${EXECLINE_VERSION}.tar.gz \
    && cd execline-${EXECLINE_VERSION} \
    && ./configure \
    && make \
    && make install \
    && make install DESTDIR=/root/s6 \
    && cd .. \
    && wget -q https://skarnet.org/software/s6/s6-${S6_VERSION}.tar.gz \
    && tar -xzvf s6-${S6_VERSION}.tar.gz \
    && cd s6-${S6_VERSION} \
    && ./configure \
    && make \
    && make install \
    && make install DESTDIR=/root/s6 \
    && cd .. \
    && tar -C /root/s6 -czf s6-bin.tar.gz . \
    && rm -rf /var/lib/apt/lists/*

# ------------------------------------------------------------------------------------

FROM debian:stretch
MAINTAINER mail@aallrd.com

ARG KANBOARD_VERSION=1.2.10

ENV DEBIAN_FRONTEND noninteractive

VOLUME /var/www/app/data
VOLUME /var/www/app/plugins
VOLUME /etc/nginx/ssl

EXPOSE 80 443

# Copy the archived S6 binaries from the first stage
COPY --from=0 /s6-bin.tar.gz .

RUN \
    # Installing the S6 binaries
    tar -xzf s6-bin.tar.gz \
    && rm -f s6-bin.tar.gz \
    # Installing the Kanboard dependencies
    && apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests \
      # Packages mentioned in the official Dockerfile
      # https://github.com/kanboard/kanboard/blob/master/Dockerfile
      curl \
      ca-certificates \
      unzip \
      ssmtp \
      mailutils \
      cron \
      # Web server package
      nginx \
      # PHP packages
      php7.0 \
      php7.0-fpm \
      php7.0-phar \
      php7.0-curl \
      php7.0-json \
      php7.0-xml \
      php7.0-common \
      php7.0-opcache \
      php7.0-zip \
      php7.0-interbase \
      php7.0-mysql \
      php7.0-pgsql \
      php7.0-sqlite3 \
      php7.0-mbstring \
      php7.0-bcmath \
      php7.0-gd \
      php7.0-mcrypt \
      php7.0-ldap \
      # Utilities
      git \
    # Cloning and installing the Kanboard sources 
    && git clone https://github.com/kanboard/kanboard.git \
    && cd /kanboard \
    && git checkout tags/v${KANBOARD_VERSION} \
    && cd .. \
    && cp -r /kanboard/docker/* / \
    && cp -r /kanboard/* /var/www/app/ \
    && rm -rf /var/www/app/docker /var/www/app/.git /kanboard \
    # Removing useless packages and cleaning up the apt cache
    && apt-get remove -y --purge git \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    # Creating the nginx user
    && useradd -s /bin/false nginx \
    # Symlinking files in order to use the kanboard provided
    # configuration and stick as much as possible to the source
    && ln -s /usr/sbin/php-fpm7.0 /usr/sbin/php-fpm7 \
    && ln -s /usr/sbin/cron /usr/sbin/crond \
    && ln -sf /etc/php7/php-fpm.conf /etc/php/7.0/fpm/php-fpm.conf

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD []
